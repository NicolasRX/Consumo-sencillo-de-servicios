import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Task } from '../models/task-model';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';



@Injectable()
export class TaskListService {


  constructor(private _http: Http) {

  }

  getAll(): Observable<Array<Task>> {

    const url = 'http://localhost:8085/task';
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({
        headers: headers
    });

    return this._http.get(url, options).map((response) =>  {
      return response.json();
    });

  }
}
