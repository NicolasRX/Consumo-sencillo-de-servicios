import { Component, OnInit } from '@angular/core';
import { TaskListService } from './services/task-list.service';
import { Task } from './models/task-model';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

    isLoading = true;
    tasks: Array<Task>;

  constructor(private taskListService: TaskListService) { }

  ngOnInit() {
    this.taskListService.getAll().subscribe(
        (data) => {
          this.tasks = data;
          this.isLoading = false;

        },
        err => {
          console.error(err);
        },
        () => {
          console.log('Finished');
        }
    );

  }

}
