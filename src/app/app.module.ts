import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TaskListComponent } from './auth/task-list/task-list.component';
import { LoaderComponent } from './common/loader/loader.component';

import { TaskListService } from './auth/task-list/services/task-list.service';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    TaskListComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
      HttpModule
  ],
  providers: [TaskListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
