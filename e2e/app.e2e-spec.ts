import { CRUDPage } from './app.po';

describe('crud App', () => {
  let page: CRUDPage;

  beforeEach(() => {
    page = new CRUDPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
